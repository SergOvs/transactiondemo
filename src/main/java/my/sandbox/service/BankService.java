package my.sandbox.service;

import my.sandbox.repository.BankManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
@Transactional
public class BankService {

    private final BankManager bankManager;

    public BankService(BankManager bankManager) {
        this.bankManager = bankManager;
    }

    public BigDecimal getAmount(String currency) {
        return bankManager.getMoney(currency);
    }

    public void addMoney(String currency, BigDecimal amount) {
        bankManager.addMoney(currency, amount);
    }

    public int closeAccount(String currency) {
        return bankManager.closeAccount(currency);
    }

    public int openAccount(String currency, BigDecimal amount) {
        return bankManager.openAccount(currency, amount);
    }
}
