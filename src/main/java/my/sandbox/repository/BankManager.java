package my.sandbox.repository;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

@Repository
public class BankManager {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public BankManager(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public BigDecimal getMoney(String currency) {
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValue("id", currency);
        return jdbcTemplate.queryForObject("select amount from money where currency = :id", namedParameters, BigDecimal.class);
    }

    public void addMoney(String currency, BigDecimal amount) {
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", currency)
                .addValue("amount", amount);
        jdbcTemplate.update("update money set amount = amount + :amount where currency = :id", namedParameters);
    }

    public List<BigDecimal> getAllCurrencyValues() {
        return jdbcTemplate.query("select amount from money", (rs, num) -> rs.getBigDecimal(1));
    }

    public BigDecimal getTotal() {
        return jdbcTemplate.queryForObject("select sum(amount) from money", Collections.emptyMap(), BigDecimal.class);
    }

    public int openAccount(String currency, BigDecimal amount) {
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", currency)
                .addValue("amount", amount);
        return jdbcTemplate.update("INSERT INTO Money (currency, amount) VALUES (:id, :amount)", namedParameters);
    }

    public int closeAccount(String currency) {
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValue("id", currency);
        return jdbcTemplate.update("delete from money where currency = :id", namedParameters);
    }
}
