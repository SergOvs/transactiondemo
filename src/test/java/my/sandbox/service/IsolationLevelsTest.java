package my.sandbox.service;

import my.sandbox.SpringJdbcConfig;
import my.sandbox.repository.BankManager;
import my.sandbox.service.helper.BankServiceParallel;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Phaser;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { SpringJdbcConfig.class})
class IsolationLevelsTest {

    @Autowired
    private BankService bankService;

    @Autowired
    private BankServiceParallel bankServiceParallel;

    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

    @BeforeAll
    static void beforeAll() {
        final ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringJdbcConfig.class);
        final NamedParameterJdbcTemplate jdbcTemplate = applicationContext.getBean(NamedParameterJdbcTemplate.class);
        jdbcTemplate.update("create table money (\n" +
                "\tcurrency char(3) primary key,\n" +
                "\tamount numeric(10,2) not null\n" +
                ")", Collections.emptyMap());
        final BankManager bankManager = applicationContext.getBean(BankManager.class);
        bankManager.openAccount("CZK", BigDecimal.ZERO);
        bankManager.openAccount("EUR", BigDecimal.TEN);
    }

    @AfterAll
    static void afterAll() {
        final ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringJdbcConfig.class);
        final NamedParameterJdbcTemplate jdbcTemplate = applicationContext.getBean(NamedParameterJdbcTemplate.class);
        jdbcTemplate.update("drop table money", Collections.emptyMap());
    }

    @Test
    @DisplayName("Lost update test")
    void lostUpdateTest() throws ExecutionException, InterruptedException {
        final BigDecimal was = bankService.getAmount("CZK");

        final CyclicBarrier ready = new CyclicBarrier(5);

        final List<Future<Boolean>> futures = executorService.invokeAll(Arrays.asList(
                () -> bankServiceParallel.addMoneyInParallel("CZK", BigDecimal.valueOf(1), ready),
                () -> bankServiceParallel.addMoneyInParallel("CZK", BigDecimal.valueOf(2), ready),
                () -> bankServiceParallel.addMoneyInParallel("CZK", BigDecimal.valueOf(3), ready),
                () -> bankServiceParallel.addMoneyInParallel("CZK", BigDecimal.valueOf(4), ready),
                () -> bankServiceParallel.addMoneyInParallel("CZK", BigDecimal.valueOf(5), ready)
        ));
        for (Future<Boolean> future : futures) {
            Assertions.assertTrue(future.get());
        }

        final BigDecimal became = bankService.getAmount("CZK");
        Assertions.assertEquals(was.add(new BigDecimal(15)), became);
    }

    @Test
    @DisplayName("Dirty read test")
    void dirtyReadTest() throws ExecutionException, InterruptedException {
        final BigDecimal was = bankService.getAmount("CZK");
        final Phaser phaser = new Phaser(2);

        final Future<Boolean> future1 = executorService.submit(() -> bankServiceParallel.addMoneyInParallelThenFail("CZK", BigDecimal.valueOf(1_000_000), phaser));
        final Future<BigDecimal> future2 = executorService.submit(() -> bankServiceParallel.getAmountForDirtyReadTest("CZK", phaser));
        Assertions.assertTrue(future1.get());
        Assertions.assertEquals(was, future2.get());

        BigDecimal became = bankService.getAmount("CZK");
        Assertions.assertEquals(was, became);
    }

    @Test
    @DisplayName("Non-repeatable read test")
    void nonRepeatableReadTest() throws ExecutionException, InterruptedException {
        BigDecimal was = bankService.getAmount("CZK");
        Phaser phaser = new Phaser(2);

        Future<List<BigDecimal>> future = executorService.submit(() -> bankServiceParallel.readMoneySeveralTimes("CZK", phaser));
        phaser.arriveAndAwaitAdvance(); // phase 0->1
        bankService.addMoney("CZK", BigDecimal.TEN);
        phaser.arrive();

        final List<BigDecimal> values = future.get();
        Assertions.assertEquals(2, values.size());
        Assertions.assertEquals(was, values.get(0));
        Assertions.assertEquals(1, new HashSet<>(values).size());

        BigDecimal became = bankService.getAmount("CZK");
        Assertions.assertEquals(was.add(BigDecimal.TEN), became);
    }

    @Test
    @DisplayName("Phantom read test")
    void phantomReadTest() throws ExecutionException, InterruptedException, BrokenBarrierException {
        bankService.closeAccount("USD");
        Phaser phaser = new Phaser(2);

        Future<List<BigDecimal>> future = executorService.submit(() -> bankServiceParallel.generateReport(phaser));
        phaser.arriveAndAwaitAdvance(); // phase 0->1
        int numOfOpened = bankService.openAccount("USD", BigDecimal.TEN);
        Assertions.assertEquals(1, numOfOpened);
        phaser.arrive();

        final List<BigDecimal> values = future.get();
        Assertions.assertEquals(2, values.size());
        Assertions.assertEquals(1, new HashSet<>(values).size());
    }

}