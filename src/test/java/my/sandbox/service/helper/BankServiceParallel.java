package my.sandbox.service.helper;

import my.sandbox.repository.BankManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Service
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class BankServiceParallel {
    @Autowired
    private BankManager bankManager;

    public boolean addMoneyInParallel(String currency, BigDecimal amount, CyclicBarrier barrier) throws InterruptedException, BrokenBarrierException {
        barrier.await();
        bankManager.addMoney(currency, amount);
        return true;
    }

    @Transactional(isolation = Isolation.DEFAULT)
    public boolean addMoneyInParallelThenFail(String currency, BigDecimal amount, Phaser phaser) throws InterruptedException {
        bankManager.addMoney(currency, amount);
        phaser.arriveAndAwaitAdvance(); // phase 0 -> 1
        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        tryAwait(phaser, 1); // phase 1 -> 2
        return true;
    }

    public BigDecimal getAmountForDirtyReadTest(String currency, Phaser phaser) throws InterruptedException {
        phaser.arriveAndAwaitAdvance(); // 0 -> 1
        final BigDecimal result = bankManager.getMoney(currency);
        phaser.arrive();
        return result;
    }

    public List<BigDecimal> readMoneySeveralTimes(String currency, Phaser phaser) throws InterruptedException {
        List<BigDecimal> result = new ArrayList<>(2);
        // first time at very beginning of transaction
        result.add(bankManager.getMoney(currency));
        phaser.arriveAndAwaitAdvance(); // phase 0 -> 1
        tryAwait(phaser, 1);
        // ... and at the end
        result.add(bankManager.getMoney(currency));
        return result;
    }

    public List<BigDecimal> generateReport(Phaser phaser) throws InterruptedException, BrokenBarrierException, TimeoutException {
        // Getting report at the beginning
        List<BigDecimal> sums = new ArrayList<>();
        sums.add(bankManager.getTotal());

        phaser.arriveAndAwaitAdvance(); // phase 0 -> 1

        tryAwait(phaser, 1); // phase 1 -> 2

        // Getting total at the end
        sums.add(bankManager.getTotal());
        return sums;
    }

    private void tryAwait(Phaser phaser, int phase) throws InterruptedException {
        phaser.arrive();
        try {
            phaser.awaitAdvanceInterruptibly(phase, 1, TimeUnit.SECONDS);
        } catch (TimeoutException e) {
            // no-op
        }
    }

}
