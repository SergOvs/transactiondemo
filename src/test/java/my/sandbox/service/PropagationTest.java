package my.sandbox.service;

import my.sandbox.SpringJdbcConfig;
import my.sandbox.repository.BankManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.util.Collections;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { SpringJdbcConfig.class})
class PropagationTest {

    @Autowired
    private BankManager bankManager;

    @Autowired
    PlatformTransactionManager transactionManager;

    @BeforeAll
    static void beforeAll() {
        final ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringJdbcConfig.class);
        final NamedParameterJdbcTemplate jdbcTemplate = applicationContext.getBean(NamedParameterJdbcTemplate.class);
        jdbcTemplate.update("create table money (\n" +
                "\tcurrency char(3) primary key,\n" +
                "\tamount numeric(10,2) not null\n" +
                ")", Collections.emptyMap());
        final BankManager bankManager = applicationContext.getBean(BankManager.class);
        bankManager.openAccount("CZK", BigDecimal.ZERO);
        bankManager.openAccount("EUR", BigDecimal.TEN);
    }

    @AfterAll
    static void afterAll() {
        final ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringJdbcConfig.class);
        final NamedParameterJdbcTemplate jdbcTemplate = applicationContext.getBean(NamedParameterJdbcTemplate.class);
        jdbcTemplate.update("drop table money", Collections.emptyMap());
    }

    @Test
    @DisplayName("REQUIRED test 1")
    void requiredTest1() {
        BigDecimal czk = bankManager.getMoney("CZK");
        BigDecimal eur = bankManager.getMoney("EUR");
        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
        transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
        transactionTemplate.setName("global");
        try {
            transactionTemplate.executeWithoutResult(
                    transactionStatus -> {
                        bankManager.addMoney("EUR", BigDecimal.TEN);

                        try {
                            TransactionTemplate inner = new TransactionTemplate(transactionManager);
                            inner.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
                            inner.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                            inner.setName("inner");
                            inner.executeWithoutResult(transactionStatus1 -> {
                                bankManager.addMoney("CZK", BigDecimal.TEN);
                                throw new RuntimeException("runtime exception");
                            });
                        } catch (Exception e) {
                            System.out.println("inner transaction failed");
                        }
                    }
            );
        } catch (Exception e) {
            System.out.println("global transaction failed");
        }

        Assertions.assertEquals(czk, bankManager.getMoney("CZK"));
        Assertions.assertEquals(eur, bankManager.getMoney("EUR"));
    }

    @Test
    @DisplayName("REQUIRED test 2")
    void requiredTest2() {
        BigDecimal czk = bankManager.getMoney("CZK");
        BigDecimal eur = bankManager.getMoney("EUR");
        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
        transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
        transactionTemplate.setName("global");
        try {
            transactionTemplate.executeWithoutResult(
                    transactionStatus -> {
                        bankManager.addMoney("EUR", BigDecimal.TEN);

                        try {
                            TransactionTemplate inner = new TransactionTemplate(transactionManager);
                            inner.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
                            inner.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                            inner.setName("inner");
                            inner.executeWithoutResult(transactionStatus1 -> {
                                bankManager.addMoney("CZK", BigDecimal.TEN);
                            });
                        } catch (Exception e) {
                            System.out.println("inner transaction failed");
                        }

                        throw new RuntimeException("runtime exception");
                    }
            );
        } catch (Exception e) {
            System.out.println("global transaction failed");
        }

        Assertions.assertEquals(czk, bankManager.getMoney("CZK"));
        Assertions.assertEquals(eur, bankManager.getMoney("EUR"));
    }

    @Test
    @DisplayName("NESTED test")
    void nestedTest() {
        BigDecimal czk = bankManager.getMoney("CZK");
        BigDecimal eur = bankManager.getMoney("EUR");
        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
        transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
        transactionTemplate.setName("global");
        try {
            transactionTemplate.executeWithoutResult(
                    transactionStatus -> {
                        bankManager.addMoney("EUR", BigDecimal.TEN);

                        try {
                            TransactionTemplate inner = new TransactionTemplate(transactionManager);
                            inner.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
                            inner.setPropagationBehavior(TransactionDefinition.PROPAGATION_NESTED);
                            inner.setName("inner");
                            inner.executeWithoutResult(transactionStatus1 -> {
                                bankManager.addMoney("CZK", BigDecimal.TEN);
                                throw new RuntimeException("runtime exception");
                            });
                        } catch (Exception e) {
                            System.out.println("inner transaction failed");
                        }
                    }
            );
        } catch (Exception e) {
            System.out.println("global transaction failed");
        }

        Assertions.assertEquals(czk, bankManager.getMoney("CZK"));
        Assertions.assertEquals(eur.add(BigDecimal.TEN), bankManager.getMoney("EUR"));
    }

    @Test
    @DisplayName("REQUIRES_NEW test")
    void requiresNewTest() {
        BigDecimal czk = bankManager.getMoney("CZK");
        BigDecimal eur = bankManager.getMoney("EUR");
        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
        transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
        transactionTemplate.setName("global");
        try {
            transactionTemplate.executeWithoutResult(
                    transactionStatus -> {
                        bankManager.addMoney("EUR", BigDecimal.TEN);

                        try {
                            TransactionTemplate inner = new TransactionTemplate(transactionManager);
                            inner.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
                            inner.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
                            inner.setName("inner");
                            inner.executeWithoutResult(transactionStatus1 -> {
                                bankManager.addMoney("CZK", BigDecimal.TEN);
                            });
                        } catch (Exception e) {
                            System.out.println("inner transaction failed");
                        }

                        throw new RuntimeException("runtime exception");
                    }
            );
        } catch (Exception e) {
            System.out.println("global transaction failed");
        }

        Assertions.assertEquals(czk.add(BigDecimal.TEN), bankManager.getMoney("CZK"));
        Assertions.assertEquals(eur, bankManager.getMoney("EUR"));
    }
}